class Palettes

  @luminanceOrder = [
    0x0, 0x6, 0x9, 0x2, 0xb, 0x4, 0x8, 0xe, 0xc, 0x5, 0xa, 0x3, 0xf, 0x7, 0xd, 0x1
  ]

  @luminanceOrderPepto = [
    0x0, 0x6, 0x9, 0xb, 0x2, 0x4, 0x8, 0xc, 0xe, 0xa, 0x5, 0xf, 0x3, 0x7, 0xd, 0x1
  ]

  @PEPTO = [
    0x000000    #0
    0xffffff    #1
    0x68372b    #2
    0x70a4b2    #3
    0x6f3d86    #4
    0x588d43    #5
    0x352879    #6
    0xb8c76f    #7
    0x6f4f25    #8
    0x433900    #9
    0x9a6759    #a
    0x444444    #b
    0x6c6c6c    #c
    0x9ad284    #d
    0x6c5eb5    #e
    0x959595    #f
  ]

  @colors136 = [
    index1: 0
    index2: 0
    mix: '#000000'
  ,
    index1: 0
    index2: 1
    mix: '#7f7f7f'
  ,
    index1: 0
    index2: 2
    mix: '#341b15'
  ,
    index1: 0
    index2: 3
    mix: '#385259'
  ,
    index1: 0
    index2: 4
    mix: '#371e43'
  ,
    index1: 0
    index2: 5
    mix: '#2c4621'
  ,
    index1: 0
    index2: 6
    mix: '#1a143c'
  ,
    index1: 0
    index2: 7
    mix: '#5c6337'
  ,
    index1: 0
    index2: 8
    mix: '#372712'
  ,
    index1: 0
    index2: 9
    mix: '#211c00'
  ,
    index1: 0
    index2: 10
    mix: '#4d332c'
  ,
    index1: 0
    index2: 11
    mix: '#222222'
  ,
    index1: 0
    index2: 12
    mix: '#363636'
  ,
    index1: 0
    index2: 13
    mix: '#4d6942'
  ,
    index1: 0
    index2: 14
    mix: '#362f5a'
  ,
    index1: 0
    index2: 15
    mix: '#4a4a4a'
  ,
    index1: 1
    index2: 1
    mix: '#ffffff'
  ,
    index1: 1
    index2: 2
    mix: '#b39b95'
  ,
    index1: 1
    index2: 3
    mix: '#b7d1d8'
  ,
    index1: 1
    index2: 4
    mix: '#b79ec2'
  ,
    index1: 1
    index2: 5
    mix: '#abc6a1'
  ,
    index1: 1
    index2: 6
    mix: '#9a93bc'
  ,
    index1: 1
    index2: 7
    mix: '#dbe3b7'
  ,
    index1: 1
    index2: 8
    mix: '#b7a792'
  ,
    index1: 1
    index2: 9
    mix: '#a19c7f'
  ,
    index1: 1
    index2: 10
    mix: '#ccb3ac'
  ,
    index1: 1
    index2: 11
    mix: '#a1a1a1'
  ,
    index1: 1
    index2: 12
    mix: '#b5b5b5'
  ,
    index1: 1
    index2: 13
    mix: '#cce8c1'
  ,
    index1: 1
    index2: 14
    mix: '#b5aeda'
  ,
    index1: 1
    index2: 15
    mix: '#cacaca'
  ,
    index1: 2
    index2: 2
    mix: '#68372b'
  ,
    index1: 2
    index2: 3
    mix: '#6c6d6e'
  ,
    index1: 2
    index2: 4
    mix: '#6b3a58'
  ,
    index1: 2
    index2: 5
    mix: '#606237'
  ,
    index1: 2
    index2: 6
    mix: '#4e2f52'
  ,
    index1: 2
    index2: 7
    mix: '#907f4d'
  ,
    index1: 2
    index2: 8
    mix: '#6b4328'
  ,
    index1: 2
    index2: 9
    mix: '#553815'
  ,
    index1: 2
    index2: 10
    mix: '#814f42'
  ,
    index1: 2
    index2: 11
    mix: '#563d37'
  ,
    index1: 2
    index2: 12
    mix: '#6a514b'
  ,
    index1: 2
    index2: 13
    mix: '#818457'
  ,
    index1: 2
    index2: 14
    mix: '#6a4a70'
  ,
    index1: 2
    index2: 15
    mix: '#7e6660'
  ,
    index1: 3
    index2: 3
    mix: '#70a4b2'
  ,
    index1: 3
    index2: 4
    mix: '#6f709c'
  ,
    index1: 3
    index2: 5
    mix: '#64987a'
  ,
    index1: 3
    index2: 6
    mix: '#526695'
  ,
    index1: 3
    index2: 7
    mix: '#94b590'
  ,
    index1: 3
    index2: 8
    mix: '#6f796b'
  ,
    index1: 3
    index2: 9
    mix: '#596e59'
  ,
    index1: 3
    index2: 10
    mix: '#858585'
  ,
    index1: 3
    index2: 11
    mix: '#5a747b'
  ,
    index1: 3
    index2: 12
    mix: '#6e888f'
  ,
    index1: 3
    index2: 13
    mix: '#85bb9b'
  ,
    index1: 3
    index2: 14
    mix: '#6e81b3'
  ,
    index1: 3
    index2: 15
    mix: '#829ca3'
  ,
    index1: 4
    index2: 4
    mix: '#6f3d86'
  ,
    index1: 4
    index2: 5
    mix: '#636564'
  ,
    index1: 4
    index2: 6
    mix: '#52327f'
  ,
    index1: 4
    index2: 7
    mix: '#93827a'
  ,
    index1: 4
    index2: 8
    mix: '#6f4655'
  ,
    index1: 4
    index2: 9
    mix: '#593b43'
  ,
    index1: 4
    index2: 10
    mix: '#84526f'
  ,
    index1: 4
    index2: 11
    mix: '#594065'
  ,
    index1: 4
    index2: 12
    mix: '#6d5479'
  ,
    index1: 4
    index2: 13
    mix: '#848785'
  ,
    index1: 4
    index2: 14
    mix: '#6d4d9d'
  ,
    index1: 4
    index2: 15
    mix: '#82698d'
  ,
    index1: 5
    index2: 5
    mix: '#588d43'
  ,
    index1: 5
    index2: 6
    mix: '#465a5e'
  ,
    index1: 5
    index2: 7
    mix: '#88aa59'
  ,
    index1: 5
    index2: 8
    mix: '#636e34'
  ,
    index1: 5
    index2: 9
    mix: '#4d6321'
  ,
    index1: 5
    index2: 10
    mix: '#797a4e'
  ,
    index1: 5
    index2: 11
    mix: '#4e6843'
  ,
    index1: 5
    index2: 12
    mix: '#627c57'
  ,
    index1: 5
    index2: 13
    mix: '#79af63'
  ,
    index1: 5
    index2: 14
    mix: '#62757c'
  ,
    index1: 5
    index2: 15
    mix: '#76916c'
  ,
    index1: 6
    index2: 6
    mix: '#352879'
  ,
    index1: 6
    index2: 7
    mix: '#767774'
  ,
    index1: 6
    index2: 8
    mix: '#523b4f'
  ,
    index1: 6
    index2: 9
    mix: '#3c303c'
  ,
    index1: 6
    index2: 10
    mix: '#674769'
  ,
    index1: 6
    index2: 11
    mix: '#3c365e'
  ,
    index1: 6
    index2: 12
    mix: '#504a72'
  ,
    index1: 6
    index2: 13
    mix: '#677d7e'
  ,
    index1: 6
    index2: 14
    mix: '#504397'
  ,
    index1: 6
    index2: 15
    mix: '#655e87'
  ,
    index1: 7
    index2: 7
    mix: '#b8c76f'
  ,
    index1: 7
    index2: 8
    mix: '#938b4a'
  ,
    index1: 7
    index2: 9
    mix: '#7d8037'
  ,
    index1: 7
    index2: 10
    mix: '#a99764'
  ,
    index1: 7
    index2: 11
    mix: '#7e8559'
  ,
    index1: 7
    index2: 12
    mix: '#92996d'
  ,
    index1: 7
    index2: 13
    mix: '#a9cc79'
  ,
    index1: 7
    index2: 14
    mix: '#929292'
  ,
    index1: 7
    index2: 15
    mix: '#a6ae82'
  ,
    index1: 8
    index2: 8
    mix: '#6f4f25'
  ,
    index1: 8
    index2: 9
    mix: '#594412'
  ,
    index1: 8
    index2: 10
    mix: '#845b3f'
  ,
    index1: 8
    index2: 11
    mix: '#594934'
  ,
    index1: 8
    index2: 12
    mix: '#6d5d48'
  ,
    index1: 8
    index2: 13
    mix: '#849054'
  ,
    index1: 8
    index2: 14
    mix: '#6d566d'
  ,
    index1: 8
    index2: 15
    mix: '#82725d'
  ,
    index1: 9
    index2: 9
    mix: '#433900'
  ,
    index1: 9
    index2: 10
    mix: '#6e502c'
  ,
    index1: 9
    index2: 11
    mix: '#433e22'
  ,
    index1: 9
    index2: 12
    mix: '#575236'
  ,
    index1: 9
    index2: 13
    mix: '#6e8542'
  ,
    index1: 9
    index2: 14
    mix: '#574b5a'
  ,
    index1: 9
    index2: 15
    mix: '#6c674a'
  ,
    index1: 10
    index2: 10
    mix: '#9a6759'
  ,
    index1: 10
    index2: 11
    mix: '#6f554e'
  ,
    index1: 10
    index2: 12
    mix: '#836962'
  ,
    index1: 10
    index2: 13
    mix: '#9a9c6e'
  ,
    index1: 10
    index2: 14
    mix: '#836287'
  ,
    index1: 10
    index2: 15
    mix: '#977e77'
  ,
    index1: 11
    index2: 11
    mix: '#444444'
  ,
    index1: 11
    index2: 12
    mix: '#585858'
  ,
    index1: 11
    index2: 13
    mix: '#6f8b64'
  ,
    index1: 11
    index2: 14
    mix: '#58517c'
  ,
    index1: 11
    index2: 15
    mix: '#6c6c6c'
  ,
    index1: 12
    index2: 12
    mix: '#6c6c6c'
  ,
    index1: 12
    index2: 13
    mix: '#839f78'
  ,
    index1: 12
    index2: 14
    mix: '#6c6590'
  ,
    index1: 12
    index2: 15
    mix: '#808080'
  ,
    index1: 13
    index2: 13
    mix: '#9ad284'
  ,
    index1: 13
    index2: 14
    mix: '#83989c'
  ,
    index1: 13
    index2: 15
    mix: '#97b38c'
  ,
    index1: 14
    index2: 14
    mix: '#6c5eb5'
  ,
    index1: 14
    index2: 15
    mix: '#8079a5'
  ,
    index1: 15
    index2: 15
    mix: '#959595'
  ]