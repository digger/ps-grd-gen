class Main

  gradients: [
    'af2b'
    '8afcd'
    '0bcf1'
    '064e31'
    '0123456789abcdef'
  ]


  convert: () ->
    gradients = @
    return


  constructor: () ->
    @generate = document.getElementById 'generate'
    @generate.addEventListener 'click', (event) =>
      @convert @text.value.split('\n')
      @text.focus()

    @populate = document.getElementById 'populate'
    @populate.addEventListener 'click', (event) =>
      @text.value += @gradients.join('\n') + '\n'
      @text.focus()

    @text = document.getElementById 'text'

    colorButtons = document.getElementById 'colorButtons'
    for i in Palettes.luminanceOrderPepto
      colorButton = document.createElement 'button'
      colorButton.style = "background: #{ColorUtils.HEXtoRGBString(Palettes.PEPTO[i])}"
      buttonText = document.createTextNode i.toString(16)
      colorButton.appendChild buttonText
      colorButtons.appendChild colorButton
      colorButton.addEventListener 'click', (event) =>
        @text.value += event.currentTarget.innerHTML
        @text.focus()

    returnButton = document.createElement 'button'
    buttonText = document.createTextNode '⏎'
    returnButton.appendChild buttonText
    colorButtons.appendChild returnButton
    returnButton.addEventListener 'click', (event) =>
      @text.value += '\n'
      @text.focus()

    @text.focus()
    return


  convert: (gradients) ->
    output = []

    for gradient in gradients
      colorIds = gradient.split ''

      # Luminance sort
      sortedColorIds = colorIds.sort (a, b) ->
        Palettes.luminanceOrderPepto.indexOf(parseInt(a, 16)) - Palettes.luminanceOrderPepto.indexOf(parseInt(b, 16))
        # Palettes.luminanceOrder.indexOf(parseInt(a, 16)) - Palettes.luminanceOrder.indexOf(parseInt(b, 16))

      numColors = sortedColorIds.length
      colorRange = 4096 / (numColors - 1)
      colors = []

      for colorId, index in sortedColorIds
        colorHex = Palettes.PEPTO[parseInt(colorId, 16)]
        colorRGB = ColorUtils.HEXtoRGBObject colorHex
        colorStop =
          midpoint: 50
          type: 'userStop'
          location: Math.round(index * colorRange)
          color:
            red: colorRGB.r
            green: colorRGB.g
            blue: colorRGB.b
        colors.push colorStop

      gradientTemplate =
        name: "[#{numColors}] #{sortedColorIds.join('')}"
        gradientForm: 'customStops'
        interpolation: 4096
        colors: colors

      output.push gradientTemplate

    # log JSON.stringify(output)
    FileSaver.saveAsTextFile JSON.stringify(output)
    return

# Bootstrap
main = new Main