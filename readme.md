# .grd gradient generator

![gradients](assets/gradients.png)

Type gradient ramp strings into the box below (or click [Populate] button for demo strings), then hit [Generate] and download your `.json` file.

Luminance sort is done automatically, so no need to worry.

Then use [this script](https://github.com/tonton-pixel/json-photoshop-scripting/tree/master/Utility-Scripts/Generate-Gradients-File) to generate `.grd` from `.json` file using Photoshop scripts.

